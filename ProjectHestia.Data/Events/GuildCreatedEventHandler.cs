﻿using DSharpPlus;
using DSharpPlus.EventArgs;

using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Events;
public class GuildCreatedEventHandler : IEventHandler<GuildCreatedEventArgs>
{
    public Task HandleEventAsync(DiscordClient sender, GuildCreatedEventArgs eventArgs)
    {
        sender.Logger.LogDebug("Guild {guild} is available.", eventArgs.Guild.Id);

        return Task.CompletedTask;
    }
}
