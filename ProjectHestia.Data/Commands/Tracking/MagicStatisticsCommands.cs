﻿using DSharpPlus;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectHestia.Data.Structures.Data.Tracking.Magic;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Interactivity.Enums;
using ProjectHestia.Data.Structures.Discord;
using DSharpPlus.Commands;
using System.ComponentModel;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Commands.Processors.SlashCommands;

namespace ProjectHestia.Data.Commands.Tracking;
public partial class TrackingCommandGroup
{
    [Description("View magic channel tracking data.")]
    [Command("magic")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.SendMessages)]
    public async Task GetMagicStatisticsAsync(SlashCommandContext ctx,
        [Description("The stat type.")]
        StatType statType,
        [Description("The stat category.")]
        MagicStatCat statCat)
    {
        ulong? guild = null;
        string? user = null;

        switch(statType)
        {
            case StatType.Server:
                guild = ctx.Guild.Id;
                break;

            case StatType.Personal:
                user = ctx.User.Id.ToString();
                break;

            case StatType.PersonalServer:
                guild = ctx.Guild.Id;
                user = ctx.User.Id.ToString();
                break;
        }

        switch (statCat)
        {
            case MagicStatCat.Entries:
                await DisplayMagicEntriesDataAsync(ctx, statType, guild, user);
                break;
            case MagicStatCat.Exits:
                await DisplayMagicExitsDataAsync(ctx, statType, guild, user);
                break;
        }
    }

    private async Task DisplayMagicEntriesDataAsync(SlashCommandContext ctx, StatType statType, ulong? guild, string? user)
    {
        await DisplayTrackingDataAsync<MagicEntryEvent>(ctx, guild, user, (items) =>
        {
            string title = statType switch
            {
                StatType.Personal => "Server Entry Data (count)",
                StatType.Server => "Personal Entry Data (count)",
                StatType.PersonalServer => "Personal Entry Data for this Server (count)",
                _ => "Unknown Data"
            };

            var ordering = items.GroupBy(e => e.UserTrackingData.DiscordUserId)
                .OrderByDescending(e => e.Count())
                .ToList();

            StringBuilder builder = new();
            for (int i = 0; i < ordering.Count; i++)
            {
                const string template = "`{0}` - <@{1}> ({2})";

                var events = ordering[i].ToList();
                var userId = events[i].UserTrackingData.DiscordUserId;

                builder.AppendFormat(template, i, userId, events.Count);
                builder.AppendLine();
            }

            var full = builder.ToString();

            return (title, full);
        });
    }

    private async Task DisplayMagicExitsDataAsync(SlashCommandContext ctx, StatType statType, ulong? guild, string? user)
    {
        await DisplayTrackingDataAsync<MagicExitEvent>(ctx, guild, user, (items) =>
        {
            string title = statType switch
            {
                StatType.Server => "Server Exit Data (normal boot, force boot)",
                StatType.Personal => "Personal Exit Data (normal boot, force boot)",
                StatType.PersonalServer => "Personal Exit Data for this Server (normal boot, force boot)",
                _ => "Unknown Data"
            };

            var ordering = items.GroupBy(e => e.UserTrackingData.DiscordUserId)
                .OrderByDescending(e => e.Count())
                .ToList();

            StringBuilder builder = new();
            for (int i = 0; i < ordering.Count; i++)
            {
                const string template = "`{0}` - <@{1}> ({2}, {3})";

                var events = ordering[i].ToList();
                var userId = events[i].UserTrackingData.DiscordUserId;
                var forceBoot = events.Count(e => e.MessageCountExpired);

                builder.AppendFormat(template, i, userId, events.Count, forceBoot);
                builder.AppendLine();
            }

            var full = builder.ToString();

            return (title, full);
        });
    }
}
