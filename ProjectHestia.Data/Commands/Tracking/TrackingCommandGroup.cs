﻿using DSharpPlus;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectHestia.Data.Services.Quote;
using ProjectHestia.Data.Services.Tracking;
using ProjectHestia.Data.Structures.Data.Tracking;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using ProjectHestia.Data.Structures.Data.Tracking.Quote;
using ProjectHestia.Data.Structures.Discord;
using DSharpPlus.Commands;
using System.ComponentModel;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Commands.Processors.SlashCommands;

namespace ProjectHestia.Data.Commands.Tracking;

[Description("Statistic Commands")]
[Command("stats")]
[RequirePermissions(DiscordPermissions.SendMessages, DiscordPermissions.SendMessages)]
public partial class TrackingCommandGroup(IDataTrackingService dataTrackingService) : CommandModule
{
    protected IDataTrackingService DataTrackingService { get; init; } = dataTrackingService;

    public enum StatType
    {
        [Description("Server")]
        Server,
        [Description("Personal")]
        Personal,
        [Description("Personal on this Server")]
        PersonalServer
    }

    public enum MagicStatCat
    {
        [Description("Entries")]
        Entries,
        [Description("Exits")]
        Exits
    }

    public enum QuoteStatCat
    {
        [Description("Created")]
        Created,
        [Description("Deleted")]
        Deleted,
        [Description("Edited")]
        Edited,
        [Description("Viewed")]
        Viewed
    }

    protected async Task DisplayTrackingDataAsync<T>(SlashCommandContext ctx, ulong? guild, string? user, 
        Func<List<T>, (string title, string full)> builder)
        where T : TrackingEvent
    {
        await ctx.DeferResponseAsync();

        var entriesRes = await DataTrackingService.GetEventData<T>(guild, user);

        if (!entriesRes.GetResult(out var res, out var err))
        {
            await ctx.DeleteResponseAsync();

            await ctx.FollowupAsync(new DiscordFollowupMessageBuilder()
                .AddEmbed(EmbedTemplates.GetErrorBuilder()
                    .WithDescription($"Failed to return data: {err[0]}")));
            return;
        }
        else if (res.Count == 0)
        {
            await ctx.DeleteResponseAsync();

            await ctx.FollowupAsync(new DiscordFollowupMessageBuilder()
                .AddEmbed(EmbedTemplates.GetStandardBuilder()
                    .WithDescription($"No data was found.")));
            return;
        }

        (var title, var full) = builder.Invoke(res);

        var pages = GeneratePages(title, full);

        await ctx.DeleteResponseAsync();

        await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages);
    }
}
