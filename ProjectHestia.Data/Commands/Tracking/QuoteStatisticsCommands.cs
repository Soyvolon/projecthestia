﻿using DSharpPlus;

using ProjectHestia.Data.Services.Tracking;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using ProjectHestia.Data.Structures.Data.Tracking.Magic;
using ProjectHestia.Data.Structures.Discord;
using ProjectHestia.Data.Structures.Data.Tracking.Quote;
using DSharpPlus.Commands;
using System.ComponentModel;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Commands.Processors.SlashCommands;

namespace ProjectHestia.Data.Commands.Tracking;
public partial class TrackingCommandGroup
{
    [Description("View quote tracking data.")]
    [Command("quote")]
    [RequirePermissions(DiscordPermissions.SendMessages)]
    public async Task GetQuoteStatisticsAsync(SlashCommandContext ctx,
        [Description("What statistics type do you want?")]
        StatType statType,

        [Description("What statistics category do you want?")]
        QuoteStatCat statCat)
    {
        ulong? guild = null;
        string? user = null;

        switch (statType)
        {
            case StatType.Server:
                guild = ctx.Guild.Id;
                break;

            case StatType.Personal:
                user = ctx.User.Id.ToString();
                break;

            case StatType.PersonalServer:
                guild = ctx.Guild.Id;
                user = ctx.User.Id.ToString();
                break;
        }

        switch (statCat)
        {
            case QuoteStatCat.Created:
                await DisplayCreatedQuoteDataAsync(ctx, statType, guild, user);
                break;
            case QuoteStatCat.Deleted:
                await DisplayDeletedQuoteDataAsync(ctx, statType, guild, user);
                break;
            case QuoteStatCat.Edited:
                await DisplayEditedQuoteDataAsync(ctx, statType, guild, user);
                break;
            case QuoteStatCat.Viewed:
                await DisplayViewedQuoteDataAsync(ctx, statType, guild, user);
                break;
        }
    }

    private async Task DisplayCreatedQuoteDataAsync(SlashCommandContext ctx, StatType statType, ulong? guild, string? user)
    {
        await DisplayTrackingDataAsync<CreateQuoteEvent>(ctx, guild, user, (items) =>
        {
            string title = statType switch
            {
                StatType.Server => "Server Quote Creation Data (count)",
                StatType.Personal => "Personal Quote Creation Data (count)",
                StatType.PersonalServer => "Personal Quote Creation Data for this Server (count)",
                _ => "Unknown Data"
            };

            var ordering = items.GroupBy(e => e.UserTrackingData.DiscordUserId)
                .OrderByDescending(e => e.Count())
                .ToList();

            StringBuilder builder = new();
            for (int i = 0; i < ordering.Count; i++)
            {
                const string template = "`{0}` - <@{1}> ({2})";

                var events = ordering[i].ToList();
                var userId = events[i].UserTrackingData.DiscordUserId;

                builder.AppendFormat(template, i, userId, events.Count);
                builder.AppendLine();
            }

            var full = builder.ToString();

            return (title, full);
        });
    }

    private async Task DisplayEditedQuoteDataAsync(SlashCommandContext ctx, StatType statType, ulong? guild, string? user)
    {
        await DisplayTrackingDataAsync<EditQuoteEvent>(ctx, guild, user, (items) =>
        {
            string title = statType switch
            {
                StatType.Server => "Server Quote Edited Data (count)",
                StatType.Personal => "Personal Quote Edited Data (count)",
                StatType.PersonalServer => "Personal Quote Edited Data for this Server (count)",
                _ => "Unknown Data"
            };

            var ordering = items.GroupBy(e => e.UserTrackingData.DiscordUserId)
                .OrderByDescending(e => e.Count())
                .ToList();

            StringBuilder builder = new();
            for (int i = 0; i < ordering.Count; i++)
            {
                const string template = "`{0}` - <@{1}> ({2})";

                var events = ordering[i].ToList();
                var userId = events[i].UserTrackingData.DiscordUserId;

                builder.AppendFormat(template, i, userId, events.Count);
                builder.AppendLine();
            }

            var full = builder.ToString();

            return (title, full);
        });
    }

    private async Task DisplayDeletedQuoteDataAsync(SlashCommandContext ctx, StatType statType, ulong? guild, string? user)
    {
        await DisplayTrackingDataAsync<DeleteQuoteEvent>(ctx, guild, user, (items) =>
        {
            string title = statType switch
            {
                StatType.Server => "Server Quote Deleted Data (count)",
                StatType.Personal => "Personal Quote Deleted Data (count)",
                StatType.PersonalServer => "Personal Quote Deleted Data for this Server (count)",
                _ => "Unknown Data"
            };

            var ordering = items.GroupBy(e => e.UserTrackingData.DiscordUserId)
                .OrderByDescending(e => e.Count())
                .ToList();

            StringBuilder builder = new();
            for (int i = 0; i < ordering.Count; i++)
            {
                const string template = "`{0}` - <@{1}> ({2})";

                var events = ordering[i].ToList();
                var userId = events[i].UserTrackingData.DiscordUserId;

                builder.AppendFormat(template, i, userId, events.Count);
                builder.AppendLine();
            }

            var full = builder.ToString();

            return (title, full);
        });
    }

    private async Task DisplayViewedQuoteDataAsync(SlashCommandContext ctx, StatType statType, ulong? guild, string? user)
    {
        await DisplayTrackingDataAsync<GetQuoteEvent>(ctx, guild, user, (items) =>
        {
            var index = "(count, random, top id, most grabbed)";
            string title = statType switch
            {
                StatType.Server => $"Server Quote Get Data {index}",
                StatType.Personal => $"Personal Quote Get Data {index}",
                StatType.PersonalServer => $"Personal Quote Get Data for this Server {index}",
                _ => "Unknown Data"
            };

            var ordering = items.GroupBy(e => e.UserTrackingData.DiscordUserId)
                .OrderByDescending(e => e.Count())
                .ToList();

            StringBuilder builder = new();
            for (int i = 0; i < ordering.Count; i++)
            {
                const string template = "`{0}` - <@{1}>\n" +
                "    c: {2} r: {3} t: {4} g: {5}";

                var events = ordering[i].ToList();
                var userId = events[i].UserTrackingData.DiscordUserId;

                var entries = events.Count;
                var randoms = events.Count(e => e.RandomGet);
                var topId = events.Max(e => e.QuoteId);
                var mostGot = events.GroupBy(e => e.QuoteId)
                    .OrderByDescending(e => e.Count())
                    .First().First().QuoteId;

                builder.AppendFormat(template, i, userId, entries,
                        randoms, topId, mostGot);
                builder.AppendLine();
            }

            var full = builder.ToString();

            return (title, full);
        });
    }
}
