﻿using DSharpPlus;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using ProjectHestia.Data.Services.Tracking;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using System.ComponentModel;
using DSharpPlus.Commands.Processors.SlashCommands;

namespace ProjectHestia.Data.Commands.Tracking;

public partial class TrackingCommandGroup
{
    [Description("Delete all tracking data for your account.")]
    [Command("delete")]
    [RequirePermissions(DiscordPermissions.SendMessages)]
    public async Task DeleteTrackingDataAsync(SlashCommandContext ctx)
    {
        var modal = new DiscordInteractionResponseBuilder()
            .WithCustomId("deletetracking")
            .WithTitle("Delete Tracking Data")
            .AddComponents(new DiscordSelectComponent("Delete Tracking Data?", "confirm", [
                    new DiscordSelectComponentOption("No", "no", isDefault: true),
                    new DiscordSelectComponentOption("Yes", "yes")
                ], false, 2, 2))
            .AddComponents(new DiscordSelectComponent("Stop Further Tracking?", "stoptrack", [
                    new DiscordSelectComponentOption("No", "no", isDefault: true),
                    new DiscordSelectComponentOption("Yes", "yes")
                ], false, 2, 2))
            .AsEphemeral();

        await ctx.RespondWithModalAsync(modal);
    }

    [Description("Disable all tracking data collection for your account.")]
    [Command("disable")]
    [RequirePermissions(DiscordPermissions.SendMessages)]
    public async Task DisableTrackingDataCollectionAsync(SlashCommandContext ctx)
        => await ModifyTrackingStatusAsync(ctx.User.Id.ToString(), false);

    [Description("Enable all tracking data collection for your account.")]
    [Command("enable")]
    [RequirePermissions(DiscordPermissions.SendMessages)]
    public async Task EnableTrackingDataCollectionAsync(SlashCommandContext ctx)
        => await ModifyTrackingStatusAsync(ctx.User.Id.ToString(), true);

    private async Task ModifyTrackingStatusAsync(string user, bool enabled)
    {
        var cfg = await DataTrackingService.GetUserTrackingConfigAsync(user);
        cfg.TrackingAllowed = enabled;
        await DataTrackingService.ModifyUserTrackingConfigAsync(cfg);
    }
}
