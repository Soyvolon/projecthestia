﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Entities;

using ProjectHestia.Data.Structures.Discord;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands.Magic;
public partial class MagicRoleCommandGroup
{
    [Command("delete")]
    public async Task DeleteMagicRoleAsync(SlashCommandContext ctx)
    {
        await ctx.DeferResponseAsync();

        var res = await _magicRoleService.DeleteMagicRole(ctx.Guild);

        if (res.GetResult(out var err))
        {
            // A success occoured.
            await ctx.EditResponseAsync(new DiscordWebhookBuilder()
                .AddEmbed(EmbedTemplates.GetSuccessBuilder()
                    .WithTitle("Deleted the magic role.")
                    .WithDescription("The magic role is now deleted. Want to add it again? Just use " +
                    "the edit command again!")));
        }
        else
        {
            // An error occoured.
            await ctx.EditResponseAsync(new DiscordWebhookBuilder()
                .AddEmbed(EmbedTemplates.GetErrorBuilder()
                    .WithTitle("Failed to delete the magic role.")
                    .WithDescription(err?.FirstOrDefault() ?? "")));
        }
    }
}
