﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Entities;

using ProjectHestia.Data.Services.Moderator;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands.Moderator;

[Description("Moderation commands.")]
[Command("mod")]
[RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
public partial class ModeratorCommandGroup : CommandModule
{
    private IModeratorService ModeratorService { get; init; }

    public ModeratorCommandGroup(IModeratorService moderatorService)
    {
        ModeratorService = moderatorService;
    }
}
