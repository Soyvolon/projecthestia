﻿using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;

using ProjectHestia.Data.Structures.Discord;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands.Moderator;
public partial class ModeratorCommandGroup
{
    [Description("Lists all strikes for a server.")]
    [Command("list")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
    public async Task ListServerStrikes(SlashCommandContext ctx)
    {
        await ctx.DeferResponseAsync();

        var allStrikesRes = await ModeratorService.GetAllStrikesInServerAsync(ctx.Guild.Id);

        if (!allStrikesRes.GetResult(out var res, out var err))
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder()
                    .AddEmbed(EmbedTemplates.GetErrorBuilder()
                        .WithTitle("Error")
                        .WithDescription($"Failed to list strikes: {string.Join('\n', err)}")));
            return;
        }

        List<string> parts = [];
        List<Page> pages = [];
        int c = 0;
        const int maxItemsPerPage = 15;
        
        int digitCount = res.Values.Max(e => e.Count).ToString().Length;
        string digitFormat = "";
        for (int i = 0; i < digitCount; i++)
            digitFormat += "0";

        string format = $"{{1:{digitFormat}}} strikes: <@{{0}}> (`{{0}}`)";

        foreach (var list in res.Values.OrderByDescending(e => e.Count))
        {
            if (c++ > maxItemsPerPage)
            {
                pages.Add(new(embed: new DiscordEmbedBuilder()
                    .WithTitle("Strikes")
                    .WithDescription(string.Join('\n', parts))));

                c = 0;
                parts.Clear();
            }

            parts.Add(string.Format(format, list.First().UserId, list.Count));
        }

        pages.Add(new(embed: new DiscordEmbedBuilder()
            .WithTitle("Strikes")
            .WithDescription(string.Join('\n', parts))));

        await ctx.DeleteResponseAsync();

        await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages);
    }
}
