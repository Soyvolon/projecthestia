﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Entities;

using ProjectHestia.Data.Structures.Discord;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands.Moderator;
public partial class ModeratorCommandGroup : CommandModule
{
    [Description("Strikes a user.")]
    [Command("strike")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
    public async Task StrikeUserAsync(SlashCommandContext ctx,
        [Description("The User to strike.")]
        DiscordMember user,

        [Description("Reason for the strike.")]
        string reason,

        [Description("The timeout time in hours for this strike.")]
        double timeout = 0,

        [Description("Do you want to send the user the message in the resaon field?")]
        bool alert = false)
    {
        await ctx.DeferResponseAsync();

        if (user is DiscordMember member)
        {

            var sRes = await ModeratorService.StrikeUserAsync(ctx.Guild.Id, member.Id, reason);

            if (sRes.GetResult(out var strike, out var err))
            {
                if (timeout > 0)
                {
                    try
                    {
                        await member.TimeoutAsync(DateTime.UtcNow.AddHours(timeout), reason.Length > 450 ? reason[..450] : reason);
                    }
                    catch (Exception ex)
                    {
                        await ctx.FollowupAsync(new DiscordFollowupMessageBuilder()
                            .WithContent($"Failed to timeout {member.Mention}: {ex.Message}"));
                    }
                }

                if (alert)
                {
                    try
                    {
                        await member.SendMessageAsync($"You received a strike on **{ctx.Guild.Name}** for: {reason}");
                    }
                    catch (Exception ex)
                    {
                        await ctx.FollowupAsync(new DiscordFollowupMessageBuilder()
                            .WithContent($"Failed to send a message to {member.Mention}: {ex.Message}"));
                    }
                }

                await ctx.EditResponseAsync(new DiscordWebhookBuilder()
                    .AddEmbed(EmbedTemplates.GetStrikeBuilder(strike, member)));

                var strikeCount = await ModeratorService.GetStrikeCountAsync(ctx.Guild.Id, member.Id);
                await ctx.FollowupAsync(new DiscordFollowupMessageBuilder()
                    .WithContent($"This is strike #{strikeCount} for {member.Mention}"));
            }
            else
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder()
                    .AddEmbed(EmbedTemplates.GetErrorBuilder()
                        .WithTitle("Error")
                        .WithDescription($"Failed to strike {member.Mention}: {string.Join('\n', err)}")));
            }
        }
        else
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder()
                .AddEmbed(EmbedTemplates.GetErrorBuilder()
                    .WithTitle("Error")
                    .WithDescription($"No discord member was able to be found for {user.Username} - `{user.Id}`")));
        }
    }
}
