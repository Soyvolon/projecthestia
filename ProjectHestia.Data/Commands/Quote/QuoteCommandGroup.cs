﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Entities;

using ProjectHestia.Data.Services.Quote;

using System.ComponentModel;

namespace ProjectHestia.Data.Commands.Quote;

[Description("Quote commands")]
[Command("quote")]
[RequirePermissions(DiscordPermissions.None, DiscordPermissions.SendMessages)]
public partial class QuoteCommandGroup : CommandModule
{
    private IQuoteService QuoteService { get; init; }

    public QuoteCommandGroup(IQuoteService quoteService)
    {
        QuoteService = quoteService;
    }
}
