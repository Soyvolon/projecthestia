﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;

using ProjectHestia.Data.Structures.Discord;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands.Quote;
public partial class QuoteCommandGroup : CommandModule
{
    [Description("List quotes")]
    [Command("list")]
    [RequirePermissions(DiscordPermissions.SendMessages)]
    public async Task ListQuotesAsync(SlashCommandContext ctx)
    {
        await ctx.DeferResponseAsync();

        var quoteRes = await QuoteService.GetQuotesAsync(ctx.Guild.Id);
        if (quoteRes.GetResult(out var quotes, out var err))
        {
            if (quotes.Count <= 0)
            {
                await ctx.DeleteResponseAsync();

                await ctx.FollowupAsync(new DiscordFollowupMessageBuilder()
                    .AddEmbed(EmbedTemplates.GetStandardBuilder()
                        .WithDescription($"No quotes to display.")));

                return;
            }

            StringBuilder builder = new();
            for (int i = 0; i < quotes.Count; i++)
            {
                var q = quotes[i];

                var part = $"{q.Author}: {q.Content}";
                part = Formatter.Strip(part);
                part = $"`{q.QuoteId}` - {part}";

                if (part.Length > 75)
                    part = part[..72] + "...";

                builder.AppendLine(part);
            }

            var full = builder.ToString();

            var pages = GeneratePages("Quotes", full);

            await ctx.DeleteResponseAsync();

            await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages);
        }
        else
        {
            await ctx.DeleteResponseAsync();

            await ctx.FollowupAsync(new DiscordFollowupMessageBuilder()
                .AddEmbed(EmbedTemplates.GetErrorBuilder()
                    .WithDescription($"Failed to retrieve quote data from this server: {err.FirstOrDefault()}")));
        }
    }
}
