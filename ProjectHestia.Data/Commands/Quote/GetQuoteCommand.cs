﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Entities;

using ProjectHestia.Data.Structures.Discord;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands.Quote;
public partial class QuoteCommandGroup : CommandModule
{
    [Description("Gets a quote by its ID")]
    [Command("get")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.SendMessages)]
    public async Task GetQuoteAsync(SlashCommandContext ctx,
        [Description("The ID of the quote to get.")]
        long quoteId)
    {
        await ctx.DeferResponseAsync();

        var quoteRes = await QuoteService.UseQuoteAsync(ctx.Guild.Id, quoteId, ctx.User.Id.ToString());

        if (!quoteRes.GetResult(out var quote, out var err))
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder()
                .AddEmbed(EmbedTemplates.GetErrorBuilder().WithDescription($"No quote found: {err[0]}")));
        }
        else
        {
            // Display the quote
            await ctx.FollowupAsync(new DiscordFollowupMessageBuilder()
                .AddEmbed(quote));
        }
    }

    [Description("Gets a random quote.")]
    [Command("random")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.SendMessages)]
    public async Task GetRandomQuoteAsync(SlashCommandContext ctx)
    {
        await ctx.DeferResponseAsync();

        var quoteRes = await QuoteService.UseRandomQuoteAsync(ctx.Guild.Id, ctx.User.Id.ToString());

        if (!quoteRes.GetResult(out var quote, out var err))
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder()
                .AddEmbed(EmbedTemplates.GetErrorBuilder().WithDescription($"No quote found: {err[0]}")));
        }
        else
        {
            // Display the quote
            await ctx.FollowupAsync(new DiscordFollowupMessageBuilder()
                .AddEmbed(quote));
        }
    }
}
