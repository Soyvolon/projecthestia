﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Entities;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands.MQuote;

public partial class ManageQuoteCommandGroup : CommandModule
{
    [Description("Add a new quote")]
    [Command("add")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
    [SlashCommandTypes(DiscordApplicationCommandType.SlashCommand, DiscordApplicationCommandType.MessageContextMenu)]
    public async Task AddQuoteAsync(SlashCommandContext ctx, DiscordMessage message = null)
    {
        DiscordInteractionResponseBuilder modal;
        if (message is not null)
        {
            modal = new DiscordInteractionResponseBuilder()
                .WithCustomId("quote")
                .WithTitle("Add Quote")
                .AddComponents(new DiscordTextInputComponent("Author", "author", "Author", message.Author?.Username))
                .AddComponents(new DiscordTextInputComponent("Saved By", "saved-by", "Who saved this quote?", ctx.User.Username))
                .AddComponents(new DiscordTextInputComponent("Content", "content", "What do you want to quote...", message.Content, style: DiscordTextInputStyle.Paragraph, required: false))
                .AddComponents(new DiscordTextInputComponent("Color", "color", "A 6 digit Hex color (# is optional)...", "#3498db", min_length: 6, max_length: 7))
                .AddComponents(new DiscordTextInputComponent("Image", "image", "A link to an image!", message.Attachments.FirstOrDefault()?.Url, required: false))
                .AsEphemeral();
        }
        else
        {
            modal = new DiscordInteractionResponseBuilder()
                .WithCustomId("quote")
                .WithTitle("Add Quote")
                .AddComponents(new DiscordTextInputComponent("Author", "author", "Author"))
                .AddComponents(new DiscordTextInputComponent("Saved By", "saved-by", "Who saved this quote..."))
                .AddComponents(new DiscordTextInputComponent("Content", "content", "What do you want to quote...", style: DiscordTextInputStyle.Paragraph, required: false))
                .AddComponents(new DiscordTextInputComponent("Color", "color", "A 6 digit Hex color (# is optional)...", "#3498db", min_length: 6, max_length: 7))
                .AddComponents(new DiscordTextInputComponent("Image", "image", "A link to an image", required: false))
                .AsEphemeral();
        }

        await ctx.RespondWithModalAsync(modal);
    }
}
