﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Entities;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands.MQuote;
public partial class ManageQuoteCommandGroup : CommandModule
{
    public enum EditType
    {
        [Description("Metadata")]
        Metadata,
        [Description("Content")]
        Content
    }

    [Description("Edit an existing quote")]
    [Command("edit")]
    public async Task EditQuoteAsync(SlashCommandContext ctx,
        [Description("ID of the quote to edit")] 
        long quoteId,

        [Description("What data are you editing?")]
        EditType editType)
    {
        var quoteRes = await QuoteService.GetQuoteAsync(ctx.Guild.Id, quoteId);

        if (!quoteRes.GetResult(out var quote, out var err))
        {
            await ctx.RespondAsync(new DiscordInteractionResponseBuilder()
                .WithContent($"No quote found to edit: {err[0]}")
                .AsEphemeral());
        }
        else
        {
            var key = quote.Key.ToString();
            var colorStr = $"{quote.Color!.Value:X}";

            var modal = editType switch
            {
                EditType.Content => new DiscordInteractionResponseBuilder()
                    .WithCustomId("quote-edit-content")
                    .WithTitle("Edit Quote")
                    .AddComponents(new DiscordTextInputComponent("Author", "author", "Author", quote.Author))
                    .AddComponents(new DiscordTextInputComponent("Content", "content", "What do you want to quote...", quote.Content, style: DiscordTextInputStyle.Paragraph, required: false))
                    .AddComponents(new DiscordTextInputComponent("Image", "image", "A link to an image!", required: false))
                    .AddComponents(new DiscordTextInputComponent("Edit Key (Do Not Change)", "key", value: key, min_length: key.Length, max_length: key.Length))
                    .AsEphemeral(),

                EditType.Metadata => new DiscordInteractionResponseBuilder()
                    .WithCustomId("quote-edit-metadata")
                    .WithTitle("Edit Quote Metadata")
                    .AddComponents(new DiscordTextInputComponent("Author", "author", "Author", quote.Author))
                    .AddComponents(new DiscordTextInputComponent("Saved By", "saved-by", "Who saved this quote?", quote.SavedBy))
                    .AddComponents(new DiscordTextInputComponent("Color", "color", "A 6 digit Hex color (# is optional)...", colorStr, min_length: 6, max_length: 7))
                    .AddComponents(new DiscordTextInputComponent("Uses", "uses", "How many uses has this quote had?", quote.Uses.ToString()))
                    .AddComponents(new DiscordTextInputComponent("Edit Key (Do Not Change)", "key", value: key, min_length: key.Length, max_length: key.Length))
                    .AsEphemeral(),

                _ => null
            };

            if (modal is not null)
                await ctx.RespondWithModalAsync(modal);
        }
    }
}
