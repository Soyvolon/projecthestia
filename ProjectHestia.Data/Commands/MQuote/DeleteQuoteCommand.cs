﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Entities;

using System.ComponentModel;

namespace ProjectHestia.Data.Commands.MQuote;
public partial class ManageQuoteCommandGroup : CommandModule
{
    [Description("Deletes a quote")]
    [Command("delete")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
    public async Task DeleteQuoteAsync(SlashCommandContext ctx,
        [Description("ID of the quote to delete.")]
        long quoteId)
    {
        var quoteRes = await QuoteService.GetQuoteAsync(ctx.Guild.Id, quoteId);

        if (!quoteRes.GetResult(out var quote, out var err))
        {
            await ctx.RespondAsync(new DiscordInteractionResponseBuilder()
                .WithContent($"No quote found to delete: {err[0]}")
                .AsEphemeral());
        }
        else
        {
            var modal = new DiscordInteractionResponseBuilder()
                .WithCustomId("quote-delete")
                .WithTitle("Press Submit to Delete")
                .AddComponents(new DiscordTextInputComponent("ID", "id", "-1", quote.QuoteId.ToString()))
                .AddComponents(new DiscordTextInputComponent("Author", "author", "Author", quote.Author))
                .AddComponents(new DiscordTextInputComponent("Content", "content", "What do you want to quote...", quote.Content, style: DiscordTextInputStyle.Paragraph, required: false))
                .AddComponents(new DiscordTextInputComponent("Image", "image", "A link to an image", quote.Image, required: false))
                .AsEphemeral();

            await ctx.RespondWithModalAsync(modal);
        }
    }
}
