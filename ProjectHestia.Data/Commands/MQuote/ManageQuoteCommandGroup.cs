﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Entities;

using ProjectHestia.Data.Services.Quote;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands.MQuote;

[Description("Manage Quote commands")]
[Command("ManageQuote")]
[RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
public partial class ManageQuoteCommandGroup : CommandModule
{
    private IQuoteService QuoteService { get; init; }

    public ManageQuoteCommandGroup(IQuoteService quoteService)
    {
        QuoteService = quoteService;
    }
}
