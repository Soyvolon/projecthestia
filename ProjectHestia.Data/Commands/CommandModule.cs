﻿using DSharpPlus;
using DSharpPlus.Interactivity;

using ProjectHestia.Data.Structures.Discord;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Commands;
public class CommandModule
{
    public List<Page> GeneratePages(string title, string full)
    {
        List<Page> pages = [];
        string part = string.Empty;
        int c = 0;
        const int maxLinesPerPage = 15;
        foreach (var line in full.Split('\n'))
        {
            if (c++ > maxLinesPerPage)
            {
                pages.Add(new(embed: EmbedTemplates.GetStandardBuilder()
                    .WithTitle(title)
                    .WithDescription(part)));

                c = 0;
                part = string.Empty;
            }

            if (c > 0)
                part += '\n';

            part += line;
        }

        pages.Add(new(embed: EmbedTemplates.GetStandardBuilder()
            .WithTitle(title)
            .WithDescription(part)));

        return pages;
    }
}
