﻿using Microsoft.EntityFrameworkCore;

using ProjectHestia.Data.Structures.Data.Guild;
using ProjectHestia.Data.Structures.Data.Magic;
using ProjectHestia.Data.Structures.Data.Moderator;
using ProjectHestia.Data.Structures.Data.Quotes;
using ProjectHestia.Data.Structures.Data.Tracking;
using ProjectHestia.Data.Structures.Data.Tracking.Magic;
using ProjectHestia.Data.Structures.Data.Tracking.Quote;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Database;
#nullable disable
public class ApplicationDbContext : DbContext
{
    internal DbSet<GuildQuote> GuildQuotes { get; set; }
    internal DbSet<GuildConfiguration> GuildConfigurations { get; set; }
    internal DbSet<UserStrike> UserStrikes { get; set; }
    internal DbSet<UserTrackingData> UserTrackingData { get; set; }
    internal DbSet<GlobalUserConfig> GlobalUserConfig { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        var guildQuotes = builder.Entity<GuildQuote>();
        guildQuotes.HasKey(e => e.Key);
        guildQuotes.HasOne(e => e.Guild)
            .WithMany(p => p.GuildQuotes)
            .HasForeignKey(e => e.GuildId);

        guildQuotes.HasAlternateKey(x => new { x.GuildId, x.QuoteId });
        guildQuotes.Ignore(e => e.Color);

        var guildConfigurations = builder.Entity<GuildConfiguration>();
        guildConfigurations.HasKey(e => e.Key);

        var userStrikes = builder.Entity<UserStrike>();
        userStrikes.HasKey(e => e.Key);
        userStrikes.HasOne(e => e.Guild)
            .WithMany(p => p.UserStrikes)
            .HasForeignKey(e => e.GuildId);

        var magicRole = builder.Entity<MagicRole>();
        magicRole.HasKey(e => e.Key);
        magicRole.HasOne(e => e.Guild)
            .WithOne(p => p.MagicRole)
            .HasForeignKey<MagicRole>(e => e.GuildId);
        magicRole.Ignore(e => e.UserMessageCounts);

        var userTrackingData = builder.Entity<UserTrackingData>();
        userTrackingData.HasKey(e => e.Key);
        userTrackingData.HasOne(e => e.GuildConfiguration)
            .WithMany(p => p.TrackingData)
            .HasForeignKey(e => e.GuildConfigurationKey);

        userTrackingData.HasMany(e => e.MagicEntryEvents)
            .WithOne(e => e.UserTrackingData)
            .HasForeignKey(e => e.UserTrackingDataKey);
        userTrackingData.HasMany(e => e.MagicExitEvents)
            .WithOne(e => e.UserTrackingData)
            .HasForeignKey(e => e.UserTrackingDataKey);
        userTrackingData.HasMany(e => e.DeleteQuoteEvents)
            .WithOne(e => e.UserTrackingData)
            .HasForeignKey(e => e.UserTrackingDataKey);
        userTrackingData.HasMany(e => e.GetQuoteEvents)
            .WithOne(e => e.UserTrackingData)
            .HasForeignKey(e => e.UserTrackingDataKey);
        userTrackingData.HasMany(e => e.EditQuoteEvents)
            .WithOne(e => e.UserTrackingData)
            .HasForeignKey(e => e.UserTrackingDataKey);
        userTrackingData.HasMany(e => e.CreateQuoteEvents)
            .WithOne(e => e.UserTrackingData)
            .HasForeignKey(e => e.UserTrackingDataKey);

        var createQuoteEvent = builder.Entity<CreateQuoteEvent>();
        createQuoteEvent.HasKey(e => e.Key);

        createQuoteEvent.HasOne(e => e.GuildQuote)
            .WithMany()
            .HasForeignKey(e => e.GuildQuoteKey);

        var deleteQuoteEvent = builder.Entity<DeleteQuoteEvent>();
        deleteQuoteEvent.HasKey(e => e.Key);

        var editQuoteEvent = builder.Entity<EditQuoteEvent>();
        editQuoteEvent.HasKey(e => e.Key);

        createQuoteEvent.HasOne(e => e.GuildQuote)
            .WithMany()
            .HasForeignKey(e => e.GuildQuoteKey);

        var getQuoteEvent = builder.Entity<GetQuoteEvent>();
        getQuoteEvent.HasKey(e => e.Key);

        getQuoteEvent.HasOne(e => e.GuildQuote)
            .WithMany()
            .HasForeignKey(e => e.GuildQuoteKey);

        var magicEntryEvent = builder.Entity<MagicEntryEvent>();
        magicEntryEvent.HasKey(e => e.Key);

        var magicExitEvent = builder.Entity<MagicExitEvent>();
        magicExitEvent.HasKey(e => e.Key);

        var globalUserConfig = builder.Entity<GlobalUserConfig>();
        globalUserConfig.HasKey(e => e.Key);
    }
}
#nullable enable