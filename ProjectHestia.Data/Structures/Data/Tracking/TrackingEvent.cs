﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Structures.Data.Tracking;
#nullable disable
public abstract class TrackingEvent : DataObject<Guid>
{
    public UserTrackingData UserTrackingData { get; set; }
    public Guid UserTrackingDataKey { get; set; }

    public DateTime Timestamp { get; set; } = DateTime.UtcNow;
}
#nullable enable
