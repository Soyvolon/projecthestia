﻿using ProjectHestia.Data.Database;
using ProjectHestia.Data.Structures.Data.Guild;
using ProjectHestia.Data.Structures.Data.Tracking.Magic;
using ProjectHestia.Data.Structures.Data.Tracking.Quote;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Structures.Data.Tracking;
public class UserTrackingData : DataObject<Guid>
{
    public string DiscordUserId { get; set; } = "";

    public List<MagicEntryEvent> MagicEntryEvents { get; set; } = [];
    public List<MagicExitEvent> MagicExitEvents { get; set; } = [];
    public List<GetQuoteEvent> GetQuoteEvents { get; set; } = [];
    public List<CreateQuoteEvent> CreateQuoteEvents {  get; set; } = [];
    public List<EditQuoteEvent> EditQuoteEvents { get; set; } = [];
    public List<DeleteQuoteEvent> DeleteQuoteEvents { get; set; } = [];

    public GuildConfiguration GuildConfiguration { get; set; }
    public ulong GuildConfigurationKey { get; set; }
}
