﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Structures.Data.Tracking.Magic;
public class MagicExitEvent : MagicEvent
{
    public bool MessageCountExpired { get; set; }
}
