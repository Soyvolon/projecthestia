﻿using ProjectHestia.Data.Structures.Data.Quotes;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Structures.Data.Tracking.Quote;
public class DeleteQuoteEvent : TrackingEvent
{
    public long QuoteId { get; set; }
}
