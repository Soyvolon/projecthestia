﻿using ProjectHestia.Data.Structures.Data.Magic;
using ProjectHestia.Data.Structures.Data.Moderator;
using ProjectHestia.Data.Structures.Data.Quotes;
using ProjectHestia.Data.Structures.Data.Tracking;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Structures.Data.Guild;
public class GuildConfiguration : DataObject<ulong>
{
    public List<GuildQuote> GuildQuotes { get; set; } = [];
    public List<UserStrike> UserStrikes { get; set; } = [];
    public List<UserTrackingData> TrackingData { get; set; } = [];
    public MagicRole? MagicRole { get; set; }
}
