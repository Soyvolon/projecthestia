﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjectHestia.Data.Migrations
{
    /// <inheritdoc />
    public partial class DataTracking : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GlobalUserConfig",
                columns: table => new
                {
                    Key = table.Column<string>(type: "text", nullable: false),
                    TrackingAllowed = table.Column<bool>(type: "boolean", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GlobalUserConfig", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "UserTrackingData",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    DiscordUserId = table.Column<string>(type: "text", nullable: false),
                    GuildConfigurationKey = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTrackingData", x => x.Key);
                    table.ForeignKey(
                        name: "FK_UserTrackingData_GuildConfigurations_GuildConfigurationKey",
                        column: x => x.GuildConfigurationKey,
                        principalTable: "GuildConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CreateQuoteEvent",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    QuoteId = table.Column<long>(type: "bigint", nullable: false),
                    GuildQuoteKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UserTrackingDataKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreateQuoteEvent", x => x.Key);
                    table.ForeignKey(
                        name: "FK_CreateQuoteEvent_GuildQuotes_GuildQuoteKey",
                        column: x => x.GuildQuoteKey,
                        principalTable: "GuildQuotes",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CreateQuoteEvent_UserTrackingData_UserTrackingDataKey",
                        column: x => x.UserTrackingDataKey,
                        principalTable: "UserTrackingData",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DeleteQuoteEvent",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    QuoteId = table.Column<long>(type: "bigint", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UserTrackingDataKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeleteQuoteEvent", x => x.Key);
                    table.ForeignKey(
                        name: "FK_DeleteQuoteEvent_UserTrackingData_UserTrackingDataKey",
                        column: x => x.UserTrackingDataKey,
                        principalTable: "UserTrackingData",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EditQuoteEvent",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    QuoteId = table.Column<long>(type: "bigint", nullable: false),
                    GuildQuoteKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UserTrackingDataKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EditQuoteEvent", x => x.Key);
                    table.ForeignKey(
                        name: "FK_EditQuoteEvent_GuildQuotes_GuildQuoteKey",
                        column: x => x.GuildQuoteKey,
                        principalTable: "GuildQuotes",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EditQuoteEvent_UserTrackingData_UserTrackingDataKey",
                        column: x => x.UserTrackingDataKey,
                        principalTable: "UserTrackingData",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GetQuoteEvent",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    RandomGet = table.Column<bool>(type: "boolean", nullable: false),
                    QuoteId = table.Column<long>(type: "bigint", nullable: false),
                    GuildQuoteKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UserTrackingDataKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GetQuoteEvent", x => x.Key);
                    table.ForeignKey(
                        name: "FK_GetQuoteEvent_GuildQuotes_GuildQuoteKey",
                        column: x => x.GuildQuoteKey,
                        principalTable: "GuildQuotes",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GetQuoteEvent_UserTrackingData_UserTrackingDataKey",
                        column: x => x.UserTrackingDataKey,
                        principalTable: "UserTrackingData",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MagicEntryEvent",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UserTrackingDataKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    RoleId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MagicEntryEvent", x => x.Key);
                    table.ForeignKey(
                        name: "FK_MagicEntryEvent_UserTrackingData_UserTrackingDataKey",
                        column: x => x.UserTrackingDataKey,
                        principalTable: "UserTrackingData",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MagicExitEvent",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    MessageCountExpired = table.Column<bool>(type: "boolean", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UserTrackingDataKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    RoleId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MagicExitEvent", x => x.Key);
                    table.ForeignKey(
                        name: "FK_MagicExitEvent_UserTrackingData_UserTrackingDataKey",
                        column: x => x.UserTrackingDataKey,
                        principalTable: "UserTrackingData",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CreateQuoteEvent_GuildQuoteKey",
                table: "CreateQuoteEvent",
                column: "GuildQuoteKey");

            migrationBuilder.CreateIndex(
                name: "IX_CreateQuoteEvent_UserTrackingDataKey",
                table: "CreateQuoteEvent",
                column: "UserTrackingDataKey");

            migrationBuilder.CreateIndex(
                name: "IX_DeleteQuoteEvent_UserTrackingDataKey",
                table: "DeleteQuoteEvent",
                column: "UserTrackingDataKey");

            migrationBuilder.CreateIndex(
                name: "IX_EditQuoteEvent_GuildQuoteKey",
                table: "EditQuoteEvent",
                column: "GuildQuoteKey");

            migrationBuilder.CreateIndex(
                name: "IX_EditQuoteEvent_UserTrackingDataKey",
                table: "EditQuoteEvent",
                column: "UserTrackingDataKey");

            migrationBuilder.CreateIndex(
                name: "IX_GetQuoteEvent_GuildQuoteKey",
                table: "GetQuoteEvent",
                column: "GuildQuoteKey");

            migrationBuilder.CreateIndex(
                name: "IX_GetQuoteEvent_UserTrackingDataKey",
                table: "GetQuoteEvent",
                column: "UserTrackingDataKey");

            migrationBuilder.CreateIndex(
                name: "IX_MagicEntryEvent_UserTrackingDataKey",
                table: "MagicEntryEvent",
                column: "UserTrackingDataKey");

            migrationBuilder.CreateIndex(
                name: "IX_MagicExitEvent_UserTrackingDataKey",
                table: "MagicExitEvent",
                column: "UserTrackingDataKey");

            migrationBuilder.CreateIndex(
                name: "IX_UserTrackingData_GuildConfigurationKey",
                table: "UserTrackingData",
                column: "GuildConfigurationKey");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CreateQuoteEvent");

            migrationBuilder.DropTable(
                name: "DeleteQuoteEvent");

            migrationBuilder.DropTable(
                name: "EditQuoteEvent");

            migrationBuilder.DropTable(
                name: "GetQuoteEvent");

            migrationBuilder.DropTable(
                name: "GlobalUserConfig");

            migrationBuilder.DropTable(
                name: "MagicEntryEvent");

            migrationBuilder.DropTable(
                name: "MagicExitEvent");

            migrationBuilder.DropTable(
                name: "UserTrackingData");
        }
    }
}
