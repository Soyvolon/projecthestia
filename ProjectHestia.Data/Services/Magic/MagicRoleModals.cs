﻿using DSharpPlus.EventArgs;
using DSharpPlus;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Services.Magic;
public class MagicRoleModals(IMagicRoleService magicRoleService) : IEventHandler<MessageCreatedEventArgs>
{
    public async Task HandleEventAsync(DiscordClient sender, MessageCreatedEventArgs eventArgs)
        => await magicRoleService.HandleEventAsync(eventArgs);
}
