﻿using DSharpPlus;
using DSharpPlus.EventArgs;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Services.Tracking;
public class DataTrackingModals(IDataTrackingService dataTrackingService) : IEventHandler<ModalSubmittedEventArgs>
{
    public async Task HandleEventAsync(DiscordClient sender, ModalSubmittedEventArgs eventArgs)
    {
        var id = eventArgs.Interaction.Data.CustomId;
        switch (id)
        {
            case "deletetracking":
                await DeleteAllTrackingData(eventArgs);
                break;
        }
    }

    private async Task DeleteAllTrackingData(ModalSubmittedEventArgs args)
    {
        var user = args.Interaction.User.Id.ToString();
        if (args.Values["confirm"] == "yes")
            await dataTrackingService.DeleteAllTrackingData(user);

        if (args.Values["stoptrack"] == "yes")
        {
            var status = await dataTrackingService.GetUserTrackingConfigAsync(user);
            status.TrackingAllowed = false;
            await dataTrackingService.ModifyUserTrackingConfigAsync(status);
        }
    }
}
