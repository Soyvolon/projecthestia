﻿using DSharpPlus.EventArgs;
using DSharpPlus;

using ProjectHestia.Data.Structures.Data.Tracking;
using ProjectHestia.Data.Structures.Data.Tracking.Magic;
using ProjectHestia.Data.Structures.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Services.Tracking;
public interface IDataTrackingService
{
    public Task AddEventAsync(ulong guild, string user, TrackingEvent eventData);
    public Task<ActionResult<List<T>>> GetEventData<T>(ulong? guild, string? user);
    public Task DeleteAllTrackingData(string user);
    public Task ModifyUserTrackingConfigAsync(GlobalUserConfig config);
    public Task<GlobalUserConfig> GetUserTrackingConfigAsync(string user);
}
