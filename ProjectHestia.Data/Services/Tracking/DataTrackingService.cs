﻿using DSharpPlus;
using DSharpPlus.EventArgs;

using Microsoft.EntityFrameworkCore;

using ProjectHestia.Data.Database;
using ProjectHestia.Data.Services.Guild;
using ProjectHestia.Data.Structures.Data.Tracking;
using ProjectHestia.Data.Structures.Data.Tracking.Magic;
using ProjectHestia.Data.Structures.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Services.Tracking;
public class DataTrackingService : IDataTrackingService
{
    private IDbContextFactory<ApplicationDbContext> DbContextFactory { get; init; }

    public DataTrackingService(IDbContextFactory<ApplicationDbContext> dbContextFactory,
        DiscordClient discord)
    {
        DbContextFactory = dbContextFactory;
    }

    public async Task AddEventAsync(ulong guildId, string user, TrackingEvent eventData)
    {
        var userCfg = await GetUserTrackingConfigAsync(user);

        if (!userCfg.TrackingAllowed)
            return;

        await using var dbContext = await DbContextFactory.CreateDbContextAsync();

        var tracking = await dbContext.UserTrackingData
            .Where(e => e.GuildConfigurationKey == guildId)
            .Where(e => e.DiscordUserId == user)
            .AsNoTrackingWithIdentityResolution()
            .FirstOrDefaultAsync();

        if (tracking is null)
        {
            tracking = new()
            {
                GuildConfigurationKey = guildId,
                DiscordUserId = user
            };

            var trackingEntry = dbContext.Add(tracking);
            await dbContext.SaveChangesAsync();
            await trackingEntry.ReloadAsync();
        }

        eventData.UserTrackingDataKey = tracking.Key;

        dbContext.Add(eventData);
        await dbContext.SaveChangesAsync();
    }

    public async Task<ActionResult<List<T>>> GetEventData<T>(ulong? guild, string? user)
    {
        var prop = typeof(UserTrackingData)
            .GetProperties()
            .Where(p => p.PropertyType == typeof(List<T>))
            .FirstOrDefault();

        if (prop is null)
            return new(false, ["Failed to get a matching data property."], null);

        await using var dbContext = await DbContextFactory.CreateDbContextAsync();

        var query = dbContext.UserTrackingData
            .Include(prop.Name);

        if (!string.IsNullOrWhiteSpace(user))
            query = query.Where(e => e.DiscordUserId == user);

        if (guild is not null)
            query = query.Where(e => e.GuildConfigurationKey == guild.Value);

        var trackingData = await query.ToListAsync();

        if (trackingData is null)
            return new(false, ["Failed to get user tracking data."], null);

        List<T> propVals = [];
        foreach (var val in trackingData)
            propVals.AddRange(prop.GetValue(val) as List<T> ?? []);

        return new(true, null, propVals);
    }

    public async Task DeleteAllTrackingData(string user)
    {
        await using var dbContext = await DbContextFactory.CreateDbContextAsync();

        _ = await dbContext.UserTrackingData
            .Where(e => e.DiscordUserId == user)
            .ExecuteDeleteAsync();
    }

    public async Task ModifyUserTrackingConfigAsync(GlobalUserConfig config)
    {
        await using var dbContext = await DbContextFactory.CreateDbContextAsync();

        dbContext.Attach(config);

        await dbContext.SaveChangesAsync();
    }

    public async Task<GlobalUserConfig> GetUserTrackingConfigAsync(string user)
    {
        await using var dbContext = await DbContextFactory.CreateDbContextAsync();

        var userObj = await dbContext.GlobalUserConfig
            .Where(e => e.Key == user)
            .AsNoTracking()
            .FirstOrDefaultAsync();

        if (userObj is null)
        {
            userObj = new()
            {
                Key = user,
            };

            dbContext.GlobalUserConfig.Add(userObj);
            await dbContext.SaveChangesAsync();
        }

        return userObj;
    }
}
