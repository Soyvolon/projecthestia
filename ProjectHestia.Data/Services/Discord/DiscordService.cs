﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using ProjectHestia.Data.Services.Magic;
using ProjectHestia.Data.Services.Quote;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Services.Discord;
public class DiscordService : IDiscordService
{
    private readonly IConfiguration _configuration;
    private readonly DiscordClient _client;
    private readonly IMagicRoleService _magicRoles;

    private InteractivityConfiguration InteractivityConfiguration { get; init; }
    
    public DiscordService(IConfiguration configuration, DiscordClient client,
        IMagicRoleService magicRoles, IServiceProvider services)
    {
        _configuration = configuration;
        _client = client;
        _magicRoles = magicRoles;

        // We need to initalize this at least once.
        _ = services.GetRequiredService<IQuoteService>();
    }

    public async Task InitializeAsync()
    {
        await _client.ConnectAsync();
    }
}
