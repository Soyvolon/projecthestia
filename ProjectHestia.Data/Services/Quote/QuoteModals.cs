﻿using DSharpPlus.EventArgs;
using DSharpPlus;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using ProjectHestia.Data.Structures.Data.Quotes;
using ProjectHestia.Data.Structures.Discord;

namespace ProjectHestia.Data.Services.Quote;
public class QuoteModals(IQuoteService quoteService) : IEventHandler<ModalSubmittedEventArgs>
{
    public async Task HandleEventAsync(DiscordClient sender, ModalSubmittedEventArgs eventArgs)
    {
        var id = eventArgs.Interaction.Data.CustomId;
        switch (id)
        {
            case "quote":
                await ModifyQuoteAsync(eventArgs);
                break;
            case "quote-delete":
                await DeleteQuoteAsync(eventArgs,
                    eventArgs.Values["id"]);
                break;
            case "quote-edit-content":
                await ModifyQuoteContentAsync(eventArgs);
                break;
            case "quote-edit-metadata":
                await ModifyQuoteMetadataAsync(eventArgs);
                break;
        }
    }

    private async Task ModifyQuoteMetadataAsync(ModalSubmittedEventArgs args)
        => await ModifyQuoteAsync(args,
            args.Values["author"],
            args.Values["saved-by"],
            null!,
            args.Values["color"],
            null!,
            args.Values["uses"],
            args.Values["key"],
            true);

    private async Task ModifyQuoteContentAsync(ModalSubmittedEventArgs args)
        => await ModifyQuoteAsync(args,
            args.Values["author"],
            null!,
            args.Values["content"],
            null!,
            args.Values["image"],
            null!,
            args.Values["key"],
            false);

    private async Task ModifyQuoteAsync(ModalSubmittedEventArgs args, string author, string savedBy, string quote, string color, string image, string uses, string? quoteKey = null, bool metadata = false)
    {
        await args.Interaction.CreateResponseAsync(DiscordInteractionResponseType.DeferredChannelMessageWithSource);

        GuildQuote? quoteData;
        List<string>? err;

        _ = long.TryParse(uses, out var usesInt);

        if (Guid.TryParse(quoteKey, out var key))
        {
            // Update quote.
            var res = await quoteService.UpdateQuoteAsync(key, author, savedBy, quote, color, image, usesInt, metadata, args.Interaction.User.Id.ToString());
            _ = res.GetResult(out quoteData, out err);
        }
        else
        {
            // Add quote.
            var res = await quoteService.AddQuoteAsync(args.Interaction.GuildId ?? 0, author, savedBy, quote, color, image, args.Interaction.User.Id.ToString());
            _ = res.GetResult(out quoteData, out err);
        }

        if (quoteData is null)
        {
            // An error occoured.
            await args.Interaction.EditOriginalResponseAsync(new DiscordWebhookBuilder()
                .AddEmbed(EmbedTemplates.GetErrorBuilder()
                    .WithTitle("Failed to edit/add a quote.")
                    .WithDescription(err?.FirstOrDefault() ?? "")));
        }
        else
        {
            // Display the quote
            await args.Interaction.EditOriginalResponseAsync(new DiscordWebhookBuilder()
                .AddEmbed(quoteData.Build()));
        }
    }

    private async Task ModifyQuoteAsync(ModalSubmittedEventArgs args)
        => await ModifyQuoteAsync(args,
            args.Values["author"],
            args.Values["saved-by"],
            args.Values["content"],
            args.Values["color"],
            args.Values["image"],
            null!);

    private async Task DeleteQuoteAsync(ModalSubmittedEventArgs args, string id)
    {
        await args.Interaction.CreateResponseAsync(DiscordInteractionResponseType.DeferredChannelMessageWithSource);

        if (long.TryParse(id, out var quoteId))
        {
            var res = await quoteService.DeleteQuoteAsync(args.Interaction.GuildId ?? 0, quoteId, args.Interaction.User.Id.ToString());

            if (res.GetResult(out var resData, out var err))
            {
                // Display the quote
                await args.Interaction.EditOriginalResponseAsync(new DiscordWebhookBuilder()
                    .AddEmbed(resData.Build()
                        .WithTitle($"Deleted Quote {quoteId}")));
            }
            else
            {
                // An error occoured.
                await args.Interaction.EditOriginalResponseAsync(new DiscordWebhookBuilder()
                    .AddEmbed(EmbedTemplates.GetErrorBuilder()
                        .WithTitle("Failed to edit/add a quote.")
                        .WithDescription(err?.FirstOrDefault() ?? "")));
            }
        }
        else
        {
            // An error occoured.
            await args.Interaction.EditOriginalResponseAsync(new DiscordWebhookBuilder()
                .AddEmbed(EmbedTemplates.GetErrorBuilder()
                    .WithTitle("Failed to delete a quote.")
                    .WithDescription($"Could not parse {id} into a valid quote ID. Make sure not to change this value.")));
        }
    }
}
