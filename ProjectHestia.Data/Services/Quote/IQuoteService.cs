﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;

using ProjectHestia.Data.Structures.Data.Quotes;
using ProjectHestia.Data.Structures.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectHestia.Data.Services.Quote;
public interface IQuoteService
{
    public Task<ActionResult<GuildQuote>> GetQuoteAsync(ulong guildId, long quoteId);
    public Task<ActionResult<GuildQuote>> UpdateQuoteAsync(Guid key, string author, string savedBy, string quote, string color, string image, long? uses, bool metadata, string updatedBy);
    public Task<ActionResult<GuildQuote>> AddQuoteAsync(ulong guild, string author, string savedBy, string quote, string color, string image, string addedBy, long? uses = null, long? forceId = null);
    public Task<ActionResult<GuildQuote>> DeleteQuoteAsync(ulong guild, long quoteId, string deletedBy);
    public Task<ActionResult<DiscordEmbedBuilder>> UseQuoteAsync(ulong guildId, long quoteId, string getBy, bool fromRandom = false);
    public Task<ActionResult<DiscordEmbedBuilder>> UseRandomQuoteAsync(ulong guildId, string getBy);
    public Task<ActionResult<List<GuildQuote>>> GetQuotesAsync(ulong guildId);
}
