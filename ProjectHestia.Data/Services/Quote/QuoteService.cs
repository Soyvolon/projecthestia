﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;

using Microsoft.EntityFrameworkCore;

using ProjectHestia.Data.Database;
using ProjectHestia.Data.Services.Guild;
using ProjectHestia.Data.Services.Tracking;
using ProjectHestia.Data.Structures.Data.Quotes;
using ProjectHestia.Data.Structures.Data.Tracking.Quote;
using ProjectHestia.Data.Structures.Discord;
using ProjectHestia.Data.Structures.Util;

using System;

namespace ProjectHestia.Data.Services.Quote;
public class QuoteService : IQuoteService
{
    private IDbContextFactory<ApplicationDbContext> DbContextFactory { get; init; }
    private IDataTrackingService DataTrackingService { get; init; }
    private DiscordClient DiscordClient { get; init; }
    private IGuildService GuildService { get; init; }
    private Random Random { get; set; }
    

    public QuoteService(IDbContextFactory<ApplicationDbContext> dbContextFactory, 
        DiscordClient discordClient, IGuildService guildService,
        IDataTrackingService dataTrackingService)
    {
        DbContextFactory = dbContextFactory;
        DiscordClient = discordClient;
        GuildService = guildService;
        DataTrackingService = dataTrackingService;

        Random = new();
    }

    public async Task<ActionResult<GuildQuote>> GetQuoteAsync(ulong guildId, long quoteId)
    {
        await using var dbContext = await DbContextFactory.CreateDbContextAsync();

        var quote = await dbContext.GuildQuotes
            .Where(x => x.QuoteId == quoteId)
            .Where(x => x.GuildId == guildId)
            .FirstOrDefaultAsync();

        if (quote is null)
            return new(false, ["Failed to find a quote."]);

        return new(true, null, quote);
    }

    public async Task<ActionResult<GuildQuote>> UpdateQuoteAsync(Guid key, string author, string savedBy, string quote, string color, string image, long? uses, bool metadata, string updatedBy)
    {
        DiscordColor? colorData = null;
        if (metadata)
        {
            try
            {
                colorData = new DiscordColor(color);
            }
            catch (Exception ex)
            {
                return new(false, [$"Failed to parse a proper color from {color}.", ex.Message]);
            }
        }

        var dbContext = await DbContextFactory.CreateDbContextAsync();
        var quoteData = await dbContext.FindAsync<GuildQuote>(key);

        if (quoteData is null)
            return new(false, ["Failed to find a quote to edit. Make sure you did not modify the edit key field."]);

        if (metadata || !string.IsNullOrEmpty(quote) || !string.IsNullOrEmpty(image))
        {
            quoteData.Update(author, savedBy, quote, colorData, image, uses, metadata);
            await dbContext.SaveChangesAsync();

            await DataTrackingService.AddEventAsync(quoteData.GuildId, updatedBy, new EditQuoteEvent()
            {
                QuoteId = quoteData.QuoteId,
                GuildQuoteKey = quoteData.Key,
            });

            return new(true, null, quoteData);
        }
        else
        {
            return new(false, ["A quote requires either an image or content."]);
        }
    }

    public async Task<ActionResult<GuildQuote>> AddQuoteAsync(ulong guild, string author, string savedBy, string quote, string color, string image, string addedBy, long? uses = null, long? forceId = null)
    {
        DiscordColor? colorData;
        try
        {
            colorData = new DiscordColor(color);
        }
        catch (Exception ex)
        {
            return new(false, [$"Failed to parse a proper color from {color}.", ex.Message]);
        }

        if (await GuildService.EnsureGuildCreated(guild))
        {
            await using var dbContex = await DbContextFactory.CreateDbContextAsync();

            if (!string.IsNullOrEmpty(quote) || !string.IsNullOrEmpty(image))
            {
                var quoteData = new GuildQuote()
                {
                    GuildId = guild,
                    Author = author,
                    SavedBy = savedBy,
                    Content = quote,
                    Color = colorData,
                    Image = image,
                    Uses = uses ?? 0,
                    LastEdit = DateTime.UtcNow
                };

                if (forceId is not null)
                {
                    quoteData.QuoteId = forceId.Value;
                }
                
                if (quoteData.QuoteId == default)
                {
                    var lastQuote = await dbContex.GuildQuotes
                        .Where(x => x.GuildId == guild)
                        .OrderBy(x => x.QuoteId)
                        .LastOrDefaultAsync();
                    quoteData.QuoteId = (lastQuote?.QuoteId ?? -1) + 1;
                }

                var dataHook = await dbContex.AddAsync(quoteData);
                await dbContex.SaveChangesAsync();

                await dataHook.ReloadAsync();

                await DataTrackingService.AddEventAsync(guild, addedBy, new CreateQuoteEvent()
                { 
                    QuoteId = quoteData.QuoteId,
                    GuildQuoteKey = quoteData.Key,
                });

                return new(true, null, quoteData);
            }
            else
            {
                return new(false, ["A quote requires either an image or content."]);
            }
        }

        return new(false, ["Failed to ensure a guild existed for this quote."]);
    }

    public async Task<ActionResult<GuildQuote>> DeleteQuoteAsync(ulong guildId, long quoteId, string deletedBy)
    {
        await using var dbContext = await DbContextFactory.CreateDbContextAsync();

        var quote = await dbContext.GuildQuotes
            .Where(x => x.QuoteId == quoteId)
            .Where(x => x.GuildId == guildId)
            .FirstOrDefaultAsync();

        if (quote is null)
            return new(false, ["No quote found to delete."]);

        dbContext.Remove(quote);
        await dbContext.SaveChangesAsync();

        await DataTrackingService.AddEventAsync(guildId, deletedBy, new DeleteQuoteEvent()
        {
            QuoteId = quoteId
        });

        return new(true, null, quote);
    }

    public async Task<ActionResult<DiscordEmbedBuilder>> UseQuoteAsync(ulong guildId, long quoteId, string getBy, bool fromRandom = false)
    {
        await using var dbContext = await DbContextFactory.CreateDbContextAsync();

        var quote = await dbContext.GuildQuotes
            .Where(x => x.QuoteId == quoteId)
            .Where(x => x.GuildId == guildId)
            .FirstOrDefaultAsync();

        if (quote is null)
            return new(false, ["Failed to find a quote to use."]);

        var data = quote.UseQuote();

        await dbContext.SaveChangesAsync();

        await DataTrackingService.AddEventAsync(guildId, getBy, new GetQuoteEvent()
        {
            QuoteId = quoteId,
            GuildQuoteKey = quote.Key,
            RandomGet = fromRandom,
        });

        return new(true, null, data);
    }

    public async Task<ActionResult<DiscordEmbedBuilder>> UseRandomQuoteAsync(ulong guildId, string getBy)
    {
        var guild = await GuildService.GetDiscordGuildConfiguration(guildId);

        if (guild is null)
            return new(false, ["No guild found to get quotes form."]);

        var quote = guild.GuildQuotes[Random.Next(0, guild.GuildQuotes.Count)];

        return await UseQuoteAsync(guildId, quote.QuoteId, getBy, true);
    }

    public async Task<ActionResult<List<GuildQuote>>> GetQuotesAsync(ulong guildId)
    {
        await using var dbContext = await DbContextFactory.CreateDbContextAsync();

        var quotes = await dbContext.GuildQuotes
            .Where(x => x.GuildId == guildId)
            .OrderBy(x => x.QuoteId)
            .ToListAsync();

        return new(true, null, quotes);
    }
}
