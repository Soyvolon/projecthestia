#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["ProjectHestia/ProjectHestia.csproj", "ProjectHestia/"]
COPY ["ProjectHestia.Components/ProjectHestia.Components.csproj", "ProjectHestia.Components/"]
COPY ["ProjectHestia.Data/ProjectHestia.Data.csproj", "ProjectHestia.Data/"]
RUN dotnet restore "./ProjectHestia/./ProjectHestia.csproj"
COPY . .
WORKDIR "/src/ProjectHestia"
RUN dotnet build "./ProjectHestia.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./ProjectHestia.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ProjectHestia.dll"]